package utils;

/**
 * Created by maximehuet on 23/04/15.
 */

        import java.security.*;
        import java.security.spec.InvalidKeySpecException;

        import javax.crypto.*;
        import javax.crypto.spec.SecretKeySpec;

        import sun.misc.*;

/**
 * Classe permettant de chiffrer et de déchiffrer les données sensibles
 */
public class AES128 {

    private static final String ALGO = "AES";
    private static final byte[] keyValue =
            new byte[] { 'T', 'h', 'e', 'B', 'e', 's', 't',
                    'S', 'e', 'c', 'r','e', 't', 'K', 'e', 'y' };

    /**
     * Permet de chiffrer du texte
     * @param Data texte à chiffrer
     * @return chiffrement via AES128 du paramètre Data
     * @throws Exception
     */
    public static String encrypt(String Data) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encVal);
        return encryptedValue;
    }

    /**
     * Permet de déchiffrer du texte
     * @param encryptedData texte chiffré à déchiffrer
     * @return déchiffrement via AES128 du paramètre encryptedData
     * @throws Exception
     */
    public static String decrypt(String encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    /**
     * Permet de générer un clé
     * @return une clé générée via le sel (keyValue) et le type d'algorithme (AES)
     * @throws Exception
     */
    private static Key generateKey() throws Exception {
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
    }

}
