package utils

import java.math.BigInteger
import java.security.SecureRandom
//remove if not needed
import scala.collection.JavaConversions._

/**
 * Objet permettant de générer un tagID de manière aléatoire
 */
object tagGenerate {

  private val random: SecureRandom = new SecureRandom()

  /**
   * Permet de générer un TagId aléatoire
   * @return tagid généré de façon aléatoire
   */
  def nextSessionId(): String = {
    new BigInteger(130, random).toString(32)
  }

}
