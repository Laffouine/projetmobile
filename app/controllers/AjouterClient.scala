package controllers

import models.{ClientAcces, _}
import play.api.libs.json._
import play.api.mvc._
import utils._

object AjouterClient extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  /**
   * Lorsque l'on reçoit une requête POST avec les paramètres corrects via la route /insererClient
   * Paramètres :
   *    nom : nom du client
   *    prenom : prénom du client
   *    tel : téléphone du client
   *    identifiant : identifiant du client
   *    nom_parking : nom du parking où le client s'inscrit
   *    mot_de_passe : mot de passe du client
   *    num : numéro de la maison du client
   *    rue : rue du client
   *    code_postal : code postal du client
   *    localité : localité du client
   *    boite : boite postale du client
   * @return insertion dans la table client, adresse et identifiant
   */
  def ajouterClient = Action(parse.tolerantFormUrlEncoded) {implicit request =>

    /*
    Données pour la table "client"
     */
    val nom = request.body.get("nom").map(_.head).getOrElse("")
    val prenom = request.body.get("prenom").map(_.head).getOrElse("")
    val tel = request.body.get("tel").map(_.head).getOrElse("")
    val identifiant = request.body.get("identifiant").map(_.head).getOrElse("")
    val nom_parking = request.body.get("nom_parking").map(_.head).getOrElse("")

    /*
    Données pour la table "identification"
     */
    val mot_de_passe = request.body.get("mot_de_passe").map(_.head).getOrElse("")

    /*
    Données pour la table "adresse"
     */
    val num = request.body.get("num").map(_.head).getOrElse("")
    val rue = request.body.get("rue").map(_.head).getOrElse("")
    val code_postal = request.body.get("code_postal").map(_.head).getOrElse("")
    val localite = request.body.get("localite").map(_.head).getOrElse("")
    val boite = request.body.get("boite").map(_.head).getOrElse("")

    /*
    Insertion dans la table "adresse"
     */
    try {
      val id_adresse = adresse_research.numberAdresse()
      val addAdresse = adresse_research.Adresse(id_adresse.length + 1, num.toInt, rue, code_postal.toInt, localite, boite)
      adresse_research.insert(addAdresse)

      /*
    Insertion dans la table "identification" avec génération d'un tagid aléatoire
   */

      val tagid = utils.tagGenerate.nextSessionId()
      println(tagid)
      try{
      val addIdentification = identification_research.Identification(identifiant, utils.AES128.encrypt(mot_de_passe), utils.AES128.encrypt(tagid))
      identification_research.insert(addIdentification)

      /*
    Insertion dans la table "client"
     */
      try {
      val addClient = client_research.Client(utils.AES128.encrypt(tagid), nom, prenom, tel.toInt, identifiant, id_adresse.length + 1, false, nom_parking)
      client_research.insert(addClient)

      Ok("true")
    }
        catch
          {
            case e: Exception => println("exception caught: " + e)
              Ok("Client incorrect")
          }
      }


      catch
        {
          case e: Exception => println("exception caught: " + e)
            Ok("Identification incorrecte")
        }
    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Adresse incorrecte")
      }
    }





}