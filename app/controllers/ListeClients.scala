package controllers

import models.{ClientAcces, _}
import play.api.libs.json._
import play.api.mvc._

object ListeClients extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()

  /**
   * Renvoie une liste de client lorsque l'on reçoit une requête POST avec les bons paramètres
   * Paramètres :
   * param_clients : string qui permet de savoir si le gestionnaire veut tous les clients ("all")
   * @return un JSON contenant la liste des clients inscrits dans le parking
   */
  def listeClients =

    Action(parse.tolerantFormUrlEncoded) { implicit request =>

      val param_clients = request.body.get("param_clients").map(_.head).getOrElse("");
      println(param_clients)
      if (param_clients == "all") {

        try {
          val clients_seq: List[client_research.Clients#TableElementType] = client_research.numberClient() // todo tous les clients select * from client

          val essai: List[JsValue] = clients_seq.map(x => client_research.writes(x))

          if (essai.size > 0) {
            Ok(Json.toJson(essai))
          }
          else {
            Ok("Aucun clients correpondants")
          }
        }
        catch {
          case e: Exception => println("exception caught: " + e)
            Ok("Problème avec nonmbre de clients")
        }
      }
      else {
        try {
          val clients_seq: List[client_research.Clients#TableElementType] = client_research.researchClientInParking() // todo select les clients qui sont parqués

          val essai: List[JsValue] = clients_seq.map(x => client_research.writes(x))

          if (clients_seq.size > 0) {
            println(Json.toJson(essai))
            Ok(Json.toJson(essai))

          }
          else {
            Ok("Aucun clients correpondants")
          }
        }
        catch {
          case e: Exception => println("exception caught: " + e)
            Ok("Problème avec nombre de clients dans le parking")
        }
      }
    }
}

      /*
      val param_identifiant = request.body.get("param_identifiant").map(_.head).getOrElse("");
      val param_mot_de_passe = request.body.get("param_mot_de_passe").map(_.head).getOrElse("");
      val param_clients = request.body.get("param_clients").map(_.head).getOrElse("");

      println(param_clients)

      try {
        val gestionnaire = gestionnaire_research.research(param_identifiant) // todo research gestionnaire via son identifiant 'param_identifiant'

        if (gestionnaire.identifiant != null) {

          try {
            val identification = identification_research.research(gestionnaire.identifiant)

            if (utils.AES128.decrypt(param_mot_de_passe) == utils.AES128.decrypt(identification.mot_de_passe)) {

              if (param_clients == "all") {

                try {
                  val clients_seq: List[client_research.Clients#TableElementType] = client_research.numberClient() // todo tous les clients select * from client

                  val essai: List[JsValue] = clients_seq.map(x => client_research.writes(x))

                  if (essai.size > 0) {
                    println("passe")
                    Ok(Json.toJson(essai))
                  }
                  else {
                    println("passe ici alors")
                    Ok("Aucun clients correpondants")
                  }
                }
                catch
                  {
                    case e: Exception => println("exception caught: " + e)
                      Ok("Problème avec nonmbre de clients")
                  }
              }
              else {
                try {
                  val clients_seq: List[client_research.Clients#TableElementType] = client_research.researchClientInParking() // todo select les clients qui sont parqués
                  val essai: List[JsValue] = clients_seq.map(x => client_research.writes(x))

                  if (clients_seq.size > 0) {

                    Ok(Json.toJson(essai))

                  }
                  else {
                    Ok("Aucun clients correpondants")
                  }
                }
                catch
                  {
                    case e: Exception => println("exception caught: " + e)
                      Ok("Problème avec nombre de clients dans le parking")
                  }
              }
            }

            else {
              Ok("mot de passe incorrect")
            }
          }
          catch
            {
              case e: Exception => println("exception caught: " + e)
                Ok("Identification gestionnaire non trouvée")
            }
        }
        else {
          Ok("identifiant incorrect")
        }
      }
      catch
        {
          case e: Exception => println("exception caught: " + e)
            Ok("Pas de gestionnaire trouvé")
        }
    }

    */




