package controllers

import models.{ClientAcces, _}
import play.api.libs.json.{Json, JsValue}
import play.api.mvc._

import scala.concurrent.Future

object PlacesLibres extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  /**
   * Permet de transformer une valeur de type Int en Json
   * @param nbPlaces nombre de places dans le parking
   * @return le nombre de places au format Json
   */
  def writes(nbPlaces : Int): JsValue = {
    Json.obj(
      "nombrePlaces" -> nbPlaces
    )

  }

  /**
   * Permet de renvoyer à l'application smartphone le nombre de places disponibles  dans le parking lorsque l'on reçoit une requête POST avec les bons paramètres
   * Paramètre :
   *    param_parking : nom du parking
   * @return Json contenant le nombre de places libres
   */
  def nbPlacesLibres = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking: String = request.body.get("param_parking").map(_.head).getOrElse("");

    try {
    val parking_find = parking_research.research(param_parking)
    val nbPlaces: Int = parking_find.nbplace

    Ok(Json.toJson(nbPlaces))
    }
    catch {
      case e: Exception => println("exception caught: " + e)
        Ok("Problème recherche parking")
    }

  }

}