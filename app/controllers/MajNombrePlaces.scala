package controllers

import models.{ClientAcces, _}
import play.api.mvc._

object MajNombrePlaces extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  /**
   * Met à jour le nombre de place lorsque la barrière est sur le statut "bloquee"
   * Paramètres :
   *    param_parking = nom du parking
   *    param_nbplace = nombre de place à mettre à jour
    * @return String contenant le statut de l'update
   */
  def updateNbPlaces = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking = request.body.get("param_parking").map(_.head).getOrElse("");
    val param_places = request.body.get("param_nbplaces").map(_.head).getOrElse("");

    val placesGestionnaire = param_places.toInt

    try {
      val parking_find: parking_research.Parkings#TableElementType = parking_research.research(param_parking)

      if (parking_find.barriere_bloquee == "bloquee") {
        parking_research.updateombrePlaces(placesGestionnaire, param_parking, parking_find)
        Ok("true")
      }
      else {
        Ok("Impossible")
      }
    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Problème avec update")
      }

  }


}