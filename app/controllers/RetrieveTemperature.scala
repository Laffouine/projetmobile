package controllers

import models.{ClientAcces, _}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import utils.tagGenerate

object RetrieveTemperature extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()

  /**
   * Permettant de transformer une valeur de type Double en Json
   * @param temperature temperature à afficher dans le Json
   * @return la valeur temperature au format Json
   */
  def writes(temperature : Double): JsValue = {
    Json.obj(
      "temperature" -> temperature
    )

  }


  def temperatureIntExt = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking = request.body.get("param_parking").map(_.head).getOrElse("")
    val param_temperature = request.body.get("param_intext").map(_.head).getOrElse("")
    try {
      val parking_find = parking_research.research(param_parking)

      if (param_temperature.equals("int")){
      val temperature = parking_find.temperature.toDouble

      val tempDegre: Double = (temperature * 0.2222) - 61.111
      Ok(Json.toJson(tempDegre))}
      else
      {
        val temperature = parking_find.temperatureExterieure.toDouble
        val tempDegre : Double = (temperature*0.2222) - 61.111
        Ok(Json.toJson(tempDegre))
      }
    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Pas de parking trouvé")
      }

  }
}
