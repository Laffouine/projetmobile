package controllers

import models.{ClientAcces, _}
import play.api.libs.json._
import play.api.mvc._

object SuppressionClient extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  /**
   * Permet de supprimer un client lorsque le gestionnaire envoie une requête via l'application smartphone
   * Paramètre :
   *      tagid : tag du client à supprimer
   * @return suppression du client dans la table client, de son adresse dans la table adresse et de son identification dans la table identification
   */
  def supprimerClient = Action (parse.tolerantFormUrlEncoded) { implicit request =>

    val tagid = request.body.get("tagid").map(_.head).getOrElse("")

    try {
      val deleteClient = client_research.research(tagid)
      /*
    Delete de son adresse
     */
      try {

        try {
          val deleteIdentification = identification_research.research(tagid)
          identification_research.delete(deleteIdentification)


          /*
    Delete du client
     */
          try {
            client_research.delete(deleteClient)



            Ok("true")
          }
          catch
            {
              case e: Exception => println("exception caught: " + e)
                Ok("Client non supprimé")
            }
        }
        catch
          {
            case e: Exception => println("exception caught: " + e)
              Ok("Identification non supprimée")
          }
      }

      catch
        {
          case e: Exception => println("exception caught: " + e)
            Ok("Adresse non supprimee")
        }
    }

    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Aucun client trouvé")
      }
  }


}