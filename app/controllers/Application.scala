package controllers


import models.ClientAcces
import models._
import play.api.db._
import play.api._
import play.api.mvc._
import scala.slick.driver.MySQLDriver.simple._
import play.api.libs.json._
import utils.tagGenerate

object Application extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  def index: Action[AnyContent] = Action {
    println(tagGenerate.nextSessionId())
    Ok(views.html.index("Your new application is ready."))
  }
}
