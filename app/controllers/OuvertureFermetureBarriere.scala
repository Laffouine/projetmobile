package controllers

import models.{ClientAcces, _}
import play.api.libs.json._
import play.api.mvc._

object OuvertureFermetureBarriere extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  /**
   * Permet de mettre à jour le statut de la barrière lorsque l'on reçoit une requête POST avec les bons paramètres
   * Paramètres :
   *    param_parking : nom du parking
   *    param_barrière : statut de la barrière (bloquée, non bloquée, ouverte)
   * @return mise à jour du statut de la barrière dans la table Parking
   */
  def barriere = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking = request.body.get("param_parking").map(_.head).getOrElse("");
    val param_barriere = request.body.get("param_barriere").map(_.head).getOrElse("");
    try {
      val parking_find: parking_research.Parkings#TableElementType = parking_research.research(param_parking)
      println(parking_find.barriere_bloquee)

      param_barriere match {
        case "non_bloquee" => parking_research.updateBarriereBloquee(param_parking, parking_find, param_barriere) // todo  changer en BD 'barriere_bloque' en false pour le parking de nom 'param_parking == FALSE
        case "bloquee" => parking_research.updateBarriereBloquee(param_parking, parking_find, param_barriere) // todo  changer en BD 'barriere_bloque' en true pour le parking de nom 'param_parking == TRUE
        case "ouverte" => parking_research.updateBarriereBloquee(param_parking, parking_find, param_barriere) //todo changer en BD
      }
      Ok("true")
    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Exception détectée")
      }
  }


}