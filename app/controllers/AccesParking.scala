package controllers
import models.{ClientAcces, _}
import play.api.libs.json._
import play.api.mvc._
import utils.AES128

object AccesParking extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()

  /**
   * Accès au parking via le RFID lorsque l'on reçoit une requête POST
   * Paramètres :
   *    param_parking : nom du parking
   *    param_tag : rfid du client voulant rentrer
   * @return
   */
  def acces = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking: String = request.body.get("param_parking").map(_.head).getOrElse("");
    val param_tag: String = request.body.get("param_tag").map(_.head).getOrElse("");


    try {
      val parking_find = parking_research.research(param_parking)


      if (parking_find != null) {

        val complet: Boolean = parking_find.complet

        val ouvert: Boolean = parking_find.ouvert

        val barriere_bloque: String = parking_find.barriere_bloquee

        if ((!(complet)) && (ouvert) && (barriere_bloque.equals("non_bloquee"))) {
          println("hhhhh")

          println(param_tag)

          try {
            val client = client_research.research(utils.AES128.decrypt(param_tag))
            if (client.tagid == null) {
              Ok("false")
            }
            else {
              println("je passe ici")

              try {
                if (parking_find.nbplace > 0){
                parking_research.updateNbPlaceDec(param_parking, parking_find)
                Ok("true")}
                else {Ok("false")}
              }
              catch
                {
                  case e: Exception => println("exception caught: " + e)
                    Ok("Incrémentation impossible")
                }
            }
          }
          catch
            {
              case e: Exception => println("exception caught: " + e)
                Ok("Client non trouvé")
            }
        }
        else {
          Ok("false")
        }
      }
      else
        Ok("false")
    }

    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Parking non trouvé")
      }
  }

}