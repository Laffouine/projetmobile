package controllers

import models.{ClientAcces, _}
import play.api.libs.json._
import play.api.mvc._

object SortieVoiture extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  /**
   * Permet de mettre à jour le nombre de place quand ne voiture sort via une requête POST avec le bon paramètre
   * Paramètre :
   *    param_parking : nom du parking
   * @return mise à jour du nombre de place dans la table Parking
   */
  def sortir = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking = request.body.get("param_parking").map(_.head).getOrElse("");

    try {

      val parking_find: parking_research.Parkings#TableElementType = parking_research.research(param_parking)
      if (parking_find.nbplace != 0) {
        if (parking_find != null) {
          try {
            parking_research.updateNbPlaceInc(param_parking, parking_find)
            Ok("true")
          }
          catch
            {
              case e: Exception => println("exception caught: " + e)
                Ok("Incrémentation impossible")
            }
        }

        else {
          Ok("false")
        }
      } else {
        Ok("false")
      }
    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Pas de parking")
      }
  }



}