package controllers

import models.{ClientAcces, _}
import play.api.mvc._

object AlerteLumiere extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()

  /**
   * Permet de mettre à jour la lumière lorsque l'on reçoit une requête POST avec les bons paramètres
   * Paramètres :
   *    param_parking : nom du parking
   *    param_lumiere : luminosité du parking
   * @return update de la lumiere dans la table Parking
   */
  def lumiere = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking = request.body.get("param_parking").map(_.head).getOrElse("")
    val param_lumiere = request.body.get("param_lumiere").map(_.head).getOrElse("")
    val parking_find: parking_research.Parkings#TableElementType = parking_research.research(param_parking)
    val lum = parking_find.temperature
    val temp = parking_find.lumiere
    println(param_lumiere)
    try {
      parking_research.updateLumiere(param_lumiere.toInt, param_parking, parking_find)
      Ok("lum")
    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Exception détectée")
      }


  }
}