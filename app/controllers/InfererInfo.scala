package controllers

import models.{ClientAcces, _}
import play.api.mvc._

object InfererInfo extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()

  /**
   * Permet d'inférer de l'information à partir de deux données (sensors). Si la température est trop élevée et que la luminosité est trop forte, alors il y a un risque de feu
   * Paramètre :
   *  param_parking : nom du parking
   * @return
   */
  def infoFeu = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking = request.body.get("param_parking").map(_.head).getOrElse("")

    try {
      val parking_find = parking_research.research(param_parking)

      val temperature = parking_find.temperature.toDouble

      val lumiere = parking_find.lumiere


      val tempDegre: Double = (temperature * 0.2222) - 61.111

      val tempFahrenheit = 1.8 * tempDegre + 32

      if (temperature > 50 && lumiere > 900) {
        Ok("alerte")
      }
      else {
        Ok("non_alerte")
      }


    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
          Ok("Pas de parking trouvé")
      }
  }

}