package controllers

import java.sql.SQLDataException

import models.{ClientAcces, _}
import play.api.libs.json._
import play.api.mvc._

object AlerteTemperature extends Controller {

  /*
  Accès aux différents modèles
   */
  val client_research: ClientAcces = new ClientAcces()
  val adresse_research = new AdresseAcces()
  val gere_research = new GereAcces()
  val gestionnaire_research = new GestionnaireAcces()
  val identification_research = new IdentificationAcces()
  val parking_research = new ParkingAcces()
  val place_research = new PlaceAcces()


  /**
   * Permet de mettre à jour la température intérieure lorsque l'on reçoit une requête POST avec les bons paramètres
   * Paramètres :
   *    param_parking : nom du parking
   *    param_temperature : temperature du parking à convertir en °C
   * @return mise à jour de la température dans la table Parking
   */
  def temperature = Action(parse.tolerantFormUrlEncoded) { implicit request =>

    val param_parking = request.body.get("param_parking").map(_.head).getOrElse("")
    val param_temperature = request.body.get("param_temperature").map(_.head).getOrElse("")

    val temperature: Int = param_temperature.toInt

    try{
      val parking_find: parking_research.Parkings#TableElementType = parking_research.research(param_parking)
      parking_research.updateTemperatureInterieure(temperature,param_parking,parking_find)
      Ok("temp")
    }
    catch
      {
        case e: Exception => println("exception caught: " + e)
        Ok("Exception détectée")
      }
  }



}