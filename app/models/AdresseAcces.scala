package models

import play.api.libs.json.{JsObject, Json}

import scala.slick.driver.MySQLDriver.simple._
/**
 * Created by maximehuet on 25/03/15.
 */
class AdresseAcces {

  /**
   * Case class pour une adresse
   * @param id_adresse id_adresse du client
   * @param num numéro de maison du client
   * @param rue rue du client
   * @param code_postal code postal du client
   * @param localite localité du client
   * @param boite boite postale du client
   */
  case class Adresse(id_adresse: Int, num: Int, rue: String, code_postal: Int, localite: String, boite: String)

  /**
   * Classe qui fait le mapping avec la table de la BD "Adresse"
   * @param tag
   */
  class Adresses(tag: Tag) extends Table[Adresse](tag, "adresse") {

    def id_adresse = column[Int]("ID_ADRESSE", O.PrimaryKey)
    def num = column[Int]("NUM", O.NotNull)
    def rue = column[String]("RUE", O.NotNull)
    def code_postal = column[Int]("CODE_POSTAL", O.NotNull)
    def localite = column[String]("LOCALITE", O.NotNull)
    def boite = column[String]("BOITE")

    // the * projection (e.g. select * ...) auto-transforms the tupled
    // column values to / from a User
    def * = (id_adresse, num, rue, code_postal, localite, boite) <>(Adresse.tupled, Adresse.unapply)


  }

  /*
  Insertion d'une adresse dans la table "Adresse"
   */
  /**
   * Insertion d'une adresse dans la table "Adresse"
   * @param adresse adresse au format de la case class à ajouter
   * @return une ligne dans Adresse est ajouté
   */
  def insert (adresse : Adresse) = {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Adresses]
      suppliers.insert(adresse)
    }
  }

  /**
   * Suppression d'une adresse dans la table "Adresse"
   * @param adresse adresse au format de la case class à supprimer
   * @return suppression d'une ligne dans la table Adresse
   */
    def delete (adresse : Adresse) =  {
      //Execute code heredef essai =
      Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

        val suppliers = TableQuery[Adresses]
        suppliers.filter(_.id_adresse === adresse.id_adresse).delete
      }
    }

  /**
   * Retourne la séquence de toutes les adresses présentes dans la BD
   * @return select¨* from Adresse
   */
    def numberAdresse () =  {
      //Execute code heredef essai =
      Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

        val suppliers = TableQuery[Adresses]
        suppliers.run
      }
    }

  /**
   * Recherche d'une adresse via l'id_adresse
   * @param id_adresse id_adresse du client à rechercher
   * @return select * from adresse where id_adresse = id_adresse
   */
    def research (id_adresse : Int): Adresses#TableElementType = {
      Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

        val suppliers = TableQuery[Adresses]
        suppliers.filter(_.id_adresse === id_adresse).list.headOption.head

      }

    }

  /**
   * Recherche de toutes les adresses présentes dans la base de données
   * @return retourne toutes les adresses présentes dans la BD
   */
  def allAdresses (): List[Adresses#TableElementType] =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Adresses]
      suppliers.run.toList

    }
  }

  /**
   * Définition permettant d'écrire un objet en JSON
   * @param adresse adresse au format de la case class
   * @return une adresse au format JSON
   */
  def writes(adresse: Adresse): JsObject = {
    Json.obj(
      "id_adresse" -> adresse.id_adresse,
      "num" -> adresse.num,
      "rue" -> adresse.rue,
      "code_postal" -> adresse.code_postal,
      "localite" -> adresse.localite,
      "boite" -> adresse.boite
    )

  }


}

