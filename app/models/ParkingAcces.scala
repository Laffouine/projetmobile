package models

import scala.slick.driver.MySQLDriver.simple._
/**
 * Created by maximehuet on 25/03/15.
 */
class ParkingAcces {

  /**
   * Case class pour un parking
   * @param nom nom du parking
   * @param ouvert booléan pour savoir si un parking est ouvert
   * @param complet booléan pour savoir si le parking est complet
   * @param nbplace nombre de places présentes dans le parking
   * @param barriere_bloquee booléan permettant si la barrière est bloquée de manière voulue ou pas
   * @param temperature temperature du parking
   * @param lumiere taux de luminosité dans le parking
   * @param temperatureExterieure temperature extérieure du parking
   */
  case class Parking (nom : String, ouvert : Boolean, complet : Boolean, nbplace : Int, barriere_bloquee : String, temperature : BigDecimal ,lumiere : BigDecimal, temperatureExterieure : BigDecimal)

  /**
   * Classe qui fait le mapping avec la table de la BD "Parking"
   * @param tag
   */
  class Parkings(tag: Tag) extends Table[Parking](tag, "parking") {

    def nom = column[String]("NOM", O.PrimaryKey)
    def ouvert = column[Boolean]("OUVERT", O.NotNull)
    def complet = column[Boolean]("COMPLET", O.NotNull)
    def nbplace = column[Int]("NBPLACE", O.NotNull)
    def barriere_bloquee = column[String]("BARRIERE_BLOQUEE", O.NotNull)
    def temperature = column[BigDecimal]("TEMPERATURE",O.NotNull)
    def lumiere = column[BigDecimal]("LUMIERE",O.NotNull)
    def temperatureExterieure = column[BigDecimal]("TEMPERATUREEXTERIEURE",O.NotNull)
    // the * projection (e.g. select * ...) auto-transforms the tupled
    // column values to / from a User
    def * = (nom, ouvert, complet, nbplace, barriere_bloquee, temperature,lumiere, temperatureExterieure) <>(Parking.tupled, Parking.unapply)


  }

  /**
   * Insertion d'un parking dans la table "Parking"
   * @param parking une ligne de la table Parking au format de la case class
   * @return insertion d'une ligne dans la table Parking
   */
  def insert (parking : Parking) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Parkings]
      suppliers.insert(parking)
    }
  }

  /**
   * Recherche d'un parking via son nom
   * @param nom nom du parking à rechercher
   * @return select * from parking where nom = nom
   */
  def research (nom : String): Parkings#TableElementType = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom).list.headOption.head

    }
  }

  /*
  Mise à jour du nombre de places disponibles dans le parking (nom)
  NombrePlace - 1
   */
  /**
   * Mise à jour du nombre de places disponibles dans le parking (nom)
   * NombrePlace - 1
   * @param nom nom du parking à mettre à jour
   * @param parking parking au format de la case class à mettre à jour
   */
  def updateNbPlaceDec(nom : String, parking: Parking) {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession  { implicit session =>

      val new_nb_place = parking.nbplace - 1
      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom)
        .map(x => (x.nbplace))
        .update((new_nb_place))
    }
  }

  /*
  Mise à jour du nombre de places disponibles dans le parking (nom)
  NombrePlace + 1
 */
  /**
   * Mise à jour du nombre de places disponibles dans le parking (nom)
   * NombrePlace + 1
   * @param nom nom du parking à mettre à jour
   * @param parking parking au format de la case class à mettre à jour
   */
  def updateNbPlaceInc(nom : String, parking: Parking) {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession  { implicit session =>

      val new_nb_place = parking.nbplace + 1
      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom)
        .map(x => (x.nbplace))
        .update((new_nb_place))
    }
  }


  /*
  Mise à jour du booléen barriere_bloquee
   */
  /**
   * Mise à jour du booléen barriere_bloquee
   * @param nom nom du parking à mettre à jour
   * @param parking parking au format de la case class à mettre à jour
   * @param statutbarriere string contenant le statut de la barriere (bloquée, non bloquée, ouverte)
   */
  def updateBarriereBloquee(nom : String, parking: Parking, statutbarriere : String) {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession  { implicit session =>

      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom)
        .map(x => (x.barriere_bloquee))
        .update((statutbarriere))
    }
  }

  /**
   * Mise à jour de la luminosité du parking
   * @param lumiere luminosité à mettre à jour dans la table Parking
   * @param nom nom du parking à mettre à jour
   * @param parking parking au format de la case class à mettre à jour
   */
  def updateLumiere(lumiere : Int, nom : String ,parking: Parking) {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession  { implicit session =>

      val new_nb_place = parking.nbplace + 1
      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom)
        .map(x => (x.lumiere))
        .update((lumiere))
    }
  }

  /**
   * Mise à jour de la température du parking
   * @param temperatureInterieure température à mettre à jour dans la table Parking
   * @param nom nom du parking à mettre à jour
   * @param parking parking au format de la case class à mettre à jour
   */
  def updateTemperatureInterieure(temperatureInterieure : Int, nom : String ,parking: Parking) {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession  { implicit session =>

      val new_nb_place = parking.nbplace + 1
      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom)
        .map(x => (x.temperature))
        .update((temperatureInterieure))
    }
  }

  /**
   * Mise à jour de la température extérieure  du parking
   * @param temperatureExterieure température extérieure à mettre à jour dans la table Parking
   * @param nom nom du parking à mettre à jour
   * @param parking parking au format de la case class à mettre à jour
   */
  def updateTemperatureExterieure(temperatureExterieure : Int, nom : String ,parking: Parking) {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession  { implicit session =>

      val new_nb_place = parking.nbplace + 1
      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom)
        .map(x => (x.temperatureExterieure))
        .update((temperatureExterieure))
    }
  }

  /**
   * Mise à jour du nombre de place lorsque la barrière est dans le statut bloquee
   * @param nombrePlaces nombre de places à mettre à jour
   * @param nom nom du parking à mettre à jour
   * @param parking parking au format de la case class à mettre à jour
   */
  def updateombrePlaces(nombrePlaces : Int, nom : String ,parking: Parking) {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession  { implicit session =>

      val new_nb_place = parking.nbplace + 1
      val suppliers = TableQuery[Parkings]
      suppliers.filter(_.nom === nom)
        .map(x => (x.nbplace))
        .update((nombrePlaces))
    }
  }
}
