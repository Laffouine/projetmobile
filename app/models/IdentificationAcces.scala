package models

import scala.slick.driver.MySQLDriver.simple._
/**
 * Created by maximehuet on 25/03/15.
 */
class IdentificationAcces {


  /**
   * Case class pour une identification
   * @param identifiant identifiant du client ou gestionnaire
   * @param mot_de_passe mot de passe du client ou du gestionnaire
   * @param tag tag du client ou du gestionnaire
   */
  case class Identification(identifiant : String, mot_de_passe : String, tag : String)


  /**
   * Classe qui fait le mapping avec la table de la BD "Identification"
   * @param tag
   */
  class Identifications(tag: Tag) extends Table[Identification](tag, "identification") {

    def identifiant = column[String]("IDENTIFIANT", O.PrimaryKey)
    def mot_de_passe = column[String]("MOT_DE_PASSE", O.NotNull)
    def tagid = column[String]("TAG", O.NotNull)
    // the * projection (e.g. select * ...) auto-transforms the tupled
    // column values to / from a User
    def * = (identifiant, mot_de_passe, tagid) <>(Identification.tupled, Identification.unapply)


  }

  /*
  Insertion d'une identification dans la table "Identification"
   */
  /**
   * Insertion d'une identification dans la table "Identification"
   * @param identification identification au format de la case class à insérer
   * @return insertion d'une ligne dans la table Identification
   */
  def insert (identification : Identification) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Identifications]
      suppliers.insert(identification)
    }
  }

  /*
  Suppression d'une identification dans la table Identification
   */
  /**
   * Suppression d'une identification dans la table Identification
   * @param identification identificaiton au format de la case class à supprimer
   * @return suppression d'une ligne dans la table Identification
   */
  def delete (identification : Identification) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Identifications]
      suppliers.filter(_.identifiant === identification.identifiant).delete
    }
  }


  /*
  Recherche d'une identification via le tagid
   */
  /**
   * Recherche d'une identification d'un client ou d'un gestionnaire via le tagid
   * @param tagid tagid du client ou du gestionnaire à rechercher
   * @return select * from Identification where tag = tagid
   */
  def research (tagid : String) = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Identifications]
      suppliers.filter(_.tagid === tagid).list.headOption.head

    }

  }
}
