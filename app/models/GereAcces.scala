package models

import scala.slick.driver.MySQLDriver.simple._
/**
 * Created by maximehuet on 25/03/15.
 */
class GereAcces {

  /**
   * case classe pour gere
   * @param tag_gestionnaire tag du gestionnaire du parking
   * @param nom_parking nom du parking qu'un gestionnaire gère
   */
  case class Gere(tag_gestionnaire: String, nom_parking : String)

  /**
   * Classe qui fait le mapping avec la table de la BD "Gere"
   * @param tag
   */
  class Geres(tag: Tag) extends Table[Gere](tag, "gere") {

    def tag_gestionnaire = column[String]("TAG_GESTIONNAIRE", O.PrimaryKey)
    def nom_parking = column[String]("NOM_PARKING", O.PrimaryKey)

    // the * projection (e.g. select * ...) auto-transforms the tupled
    // column values to / from a User
    def * = (tag_gestionnaire,nom_parking) <>(Gere.tupled, Gere.unapply)


  }

  /**
   * Insertion d'une ligne dans la table "Gere"
   * @param gere une ligne de la table Gere au format de la case class
   * @return insertion d'une ligne dans la table Gere
   */
  def insert (gere : Gere) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Geres]
      suppliers.insert(gere)
    }
  }

  /**
   * Recherche dans la table "Gere" via le tag_gestionnaire
   * @param tag_gestionnaire tag du gestionnaire à rechercher dans la table Gere
   * @return select * from gere where tag_gestionnaire = tag_gestionnaire
   */
  def researchByTag (tag_gestionnaire : String) = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Geres]
      suppliers.filter(_.tag_gestionnaire === tag_gestionnaire).list.headOption.head

    }

  }

  /**
   * Recherche dans la table "Gere" via le nom_parking
   * @param nom_parking nom du parking à recherhcer dans la table Gere
   * @return select * from gere where nom_parking = nom_parking
   */
  def researchByNom (nom_parking : String) = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Geres]
      suppliers.filter(_.nom_parking === nom_parking).list.headOption.head

    }

  }
}

