package models

import play.api.libs.json._
import play.api.libs.json.Writes._
import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.{ProvenShape, ForeignKeyQuery}
/**
 * Created by maximehuet on 25/03/15.
 */
  class ClientAcces {

  /**
   * Case class pour une client
   * @param tagid tagid du client (rfid permettant d'accéder au parking)
   * @param nom nom du client
   * @param prenom prenom du client
   * @param tel telephone du client
   * @param identifiant identifiant du client (utilisation ultérieure pour un site internet)
   * @param id_adresse id_adresse du client
   * @param in_parking booléen pour savoir si le client est dans le parking ou non
   * @param nom_parking nom du parking où il est inscrit
   */
  case class Client(tagid: String, nom: String, prenom: String, tel: Int, identifiant: String, id_adresse: Int, in_parking : Boolean, nom_parking : String)

  /**
   * Classe qui fait le mapping avec la table de la BD "Client"
   * @param tag
   */
  class Clients(tag: Tag) extends Table[Client](tag, "client") {

    def tagid = column[String]("TAG", O.PrimaryKey)
    def nom = column[String]("NOM", O.NotNull)
    def prenom = column[String]("PRENOM", O.NotNull)
    def tel = column[Int]("TEL", O.NotNull)
    def identifiant = column[String]("IDENTIFIANT", O.NotNull)
    def id_adresse = column[Int]("ID_ADRESSE", O.NotNull)
    def in_parking = column[Boolean]("IN_PARKING",O.NotNull)
    def nom_parking = column[String]("NOM_PARKING")

    // the * projection (e.g. select * ...) auto-transforms the tupled
    // column values to / from a User
    def * = (tagid, nom, prenom, tel, identifiant, id_adresse,in_parking,nom_parking) <>(Client.tupled, Client.unapply)


  }

  /**
   * Insertion d'un client dans la table "Client"
   * @param clientInsertion client au format de la case class à insérer
   * @return insertion d'une ligne dans la table Client
   */
  def insert (clientInsertion : Client) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Clients]
      suppliers.insert(clientInsertion)
    }
  }

  /*
  Suppression d'un client dans la table "Client"
   */
  /**
   * Suppression d'un client dans la table "Client"
   * @param clientSupprimer client au format de la case class à supprimer
   * @return suppression d'une ligne dans la table Client
   */
  def delete (clientSupprimer : Client) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Clients]
      suppliers.filter(_.tagid === clientSupprimer.tagid).delete
    }
  }

  /**
   * Recherche d'un client via son tagid
   * @param tagid tagid du client à supprimer
   * @return select * from client where tag = tagid
   */
  def research (tagid : String): Clients#TableElementType = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Clients]
      suppliers.filter(_.tagid === tagid).list.headOption.head

    }

  }

  /**
   * Retourne la list de tous les clients présents dans le parking
   * @return select * from client where in_parking = true
   */
  def researchClientInParking (): List[Clients#TableElementType] = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Clients]
      suppliers.run.filter(_.in_parking.==(true) ).toList


    }

  }

  /*
  Retourne la séquence dans tous les clients présents dans la BD
   */
  /**
   * Retourne la list de tous les clients présents dans la BD
   * @return select * from Client
   */
  def numberClient (): List[Clients#TableElementType] =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Clients]
      suppliers.run.toList

    }
  }

  /**
   * Définition permettant d'écrire un objet en JSON
   * @param client client au format de la case class
   * @return un client au format JSON
   */
  def writes(client: Client): JsObject = {
    Json.obj(
      "tag" -> client.tagid,
      "nom" -> client.nom,
      "prenom" -> client.prenom,
      "tel" -> client.tel,
      "identifiant" -> client.identifiant,
      "id_adresse" -> client.id_adresse,
      "in_parking" -> client.in_parking,
      "nom_parking" -> client.nom_parking
    )

  }


  /**
   * Définition permettant de lire un objet de type JSON
   * @param json json d'un client à lire
   * @return un client au format de la case class
   */
  def reads(json: JsValue): JsSuccess[Client] = {
    JsSuccess(Client(
      (json \ "tag").as[String],
      (json \ "nom").as[String],
      (json \ "prenom").as[String],
      (json \ "tel").as[Int],
      (json \ "identifiant").as[String],
      (json \ "id_adresse").as[Int],
      (json \ "in_parking").as[Boolean],
      (json \ "nom_parking").as[String]
    ))
  }


}
