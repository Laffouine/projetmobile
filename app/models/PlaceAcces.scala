package models

import scala.slick.driver.MySQLDriver.simple._
/**
 * Created by maximehuet on 25/03/15.
 */
class PlaceAcces {

  /**
   * Case class pour une place
   * @param id id de la place dans la parking
   * @param occupé booléan pour savoir si une place est occupée ou pas
   * @param handicap booléan permettant de savoir si une place est une place handicapée ou pas
   * @param nom_parking nom du parking où se trouve la place
   */
  case class Place (id :Int, occupé : Boolean, handicap : Boolean, nom_parking : String )

  /**
   * Classe qui fait le mapping avec la table de la BD "Place"
   * @param tag
   */
  class Places(tag: Tag) extends Table[Place](tag, "place") {

    def id = column[Int]("NOM", O.PrimaryKey)
    def occupé = column[Boolean]("OUVERT", O.NotNull)
    def handicap = column[Boolean]("COMPLET", O.NotNull)
    def nom_parking = column[String]("NBPLACE", O.NotNull)
    // the * projection (e.g. select * ...) auto-transforms the tupled
    // column values to / from a User
    def * = (id , occupé, handicap, nom_parking) <>(Place.tupled, Place.unapply)


  }

  /**
   * Insert d'une place dans la table "Place"
   * @param place une ligne de la table place au format de la case class
   * @return insertion d'une ligne dans la table Place
   */
  def insert (place : Place) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Places]
      suppliers.insert(place)
    }
  }

  /*
  Recherche d'une place via sont id dans la table "Place"
   */
  /**
   * Recherche d'une place via sont id dans la table "Place"
   * @param id id de la place à rechercher
   * @return select * from place where id = id
   */
  def research (id : Int) = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Places]
      suppliers.filter(_.id === id).list.headOption.head

    }

  }
}
