package models

import scala.slick.driver.MySQLDriver.simple._
/**
 * Created by maximehuet on 25/03/15.
 */
class GestionnaireAcces {

  /**
   * Case class pour un gestionnaire
   * @param tagid tag du gestionnaire
   * @param nom nom du gestionnaire
   * @param prenom prenom du gestionnaire
   * @param tel téléphone du gestionnaire
   * @param id_adresse id adresse du gestionnaire
   * @param identifiant identifiant du gestionnaire
   */
  case class Gestionnaire (tagid :String, nom : String, prenom : String, tel : Int , id_adresse : Int, identifiant : String)


  /**
   * Classe qui fait le mapping avec la table de la BD "Gestionnaire"
   * @param tag
   */
  class Gestionnaires(tag: Tag) extends Table[Gestionnaire](tag, "gestionnaire") {

    def tagid = column[String]("TAG", O.PrimaryKey)
    def nom = column[String]("NOM", O.NotNull)
    def prenom = column[String]("PRENOM", O.NotNull)
    def tel = column[Int]("TEL", O.NotNull)
    def id_adresse = column[Int]("ID_ADRESSE", O.NotNull)
    def identifiant = column[String]("IDENTIFIANT", O.NotNull)

    // the * projection (e.g. select * ...) auto-transforms the tupled
    // column values to / from a User
    def * = (tagid, nom, prenom, tel, id_adresse, identifiant) <>(Gestionnaire.tupled, Gestionnaire.unapply)


  }


  /**
   * Insertion d'un gestionnaire dans la table "Gestionnaire"
   * @param gestionnaire gestionnaire au format de la case class à insérer
   * @return insertion d'une ligne dans la table Gestionnaire
   */
  def insert (gestionnaire : Gestionnaire) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Gestionnaires]
      suppliers.insert(gestionnaire)
    }

  }

  /**
   * Suppression d'un gestionnaire dans la table "Gestionnaire"
   * @param gestionnaire gestionnaire au format de la case class à supprimer
   * @return suppression d'une ligne dans la table Gestionnaire
   */
  def delete (gestionnaire : Gestionnaire) =  {
    //Execute code heredef essai =
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Gestionnaires]
      suppliers.filter(_.tagid === gestionnaire.tagid).delete
    }
  }

  /**
   *
   * @param tagid tag du gestionnaire à rechercher dans la table Gestionnaire
   * @return select * from Gestionnaire where tag = tagid
   */
  def research (tagid : String) = {
    Database.forURL("jdbc:mysql://db4free.net:3306/conceptionmobile", driver = "com.mysql.jdbc.Driver", user = "conceptionmobile", password = "conceptionmobile").withSession { implicit session =>

      val suppliers = TableQuery[Gestionnaires]
      suppliers.filter(_.tagid === tagid).list.headOption.head

    }

  }

}
