name := "ProjetMobile"

version := "1.0"

lazy val `projetmobile` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq( jdbc , anorm , cache , ws, "mysql" % "mysql-connector-java" % "5.1.18", "com.typesafe.slick" %% "slick" % "2.1.0" ,
  "com.typesafe.play" %% "play-ws" % "2.3.1", "org.jasypt" % "jasypt" % "1.9.0" , "postgresql" % "postgresql" % "8.4-702.jdbc4")

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  